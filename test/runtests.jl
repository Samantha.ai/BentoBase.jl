using BentoBase
import BentoBase: default_widget
using Bento
using Base.Test

struct Thing1
  a::Int
  b::Float64
  c::String
  d::Bool
end

# Effectively defines all unspecified default widgets
default_widget(x::Any) = x

function default_widget(thing1::Thing1)
  layout = @layout(
    @splitv(
      @splith(
        @default(thing1.a),
        @default(thing1.b)
      ),
      @splith(
        @default(thing1.c),
        @default(thing1.d)
      )
    )
  )
end

thing1 = Thing1(1,2.3,"lol",true)
@test default_widget(thing1) isa Bento.BoardLayout
