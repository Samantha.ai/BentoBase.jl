__precompile__(true)

module BentoBase

export @default_widget, @default, @group, @pathref, @layout, @splitv, @splith, @plot, @label
export default_widget, _default_widget, widget_group, default_pathref, default_style, default_layout, default_plot, default_label
export LayoutStyle, PlotStyle

struct LayoutStyle{S}
  style::Val{S}
end
LayoutStyle(sym::Symbol) = LayoutStyle(Val{sym}())

struct PlotStyle{S}
  style::Val{S}
end
PlotStyle(sym::Symbol) = PlotStyle(Val{sym}())

widget_group() = error("No widget group defined")
default_pathref() = error("No default pathref defined")
default_style(x) = error("No default style defined for type $x")
default_style() = default_style(nothing)
default_layout(style::LayoutStyle, x...) = error("No default layout defined for style $(style.style)")
default_layout(x...) = default_layout(LayoutStyle(:default), x...)
# TODO: Support kwargs
default_plot(style::PlotStyle, x...) = error("No default plot defined for style $(style.style)")
default_plot(x...) = default_plot(PlotStyle(:default), x...)
default_label(x) = error("No default label defined for type $(typeof(x))")

_default_widget(x) = error("No default widget defined for type $(typeof(x))")
default_widget(x) = error("Bento not loaded")

macro default_widget(name, t, expr)
  quote
    function BentoBase._default_widget($(esc(name)), ::Type{T} where T<:$(esc(t)))
      $(esc(expr))
    end
  end
end

macro pathref(x)
  :(default_pathref($(esc(x))))
end

macro layout(thing)
  :(default_layout($(esc(thing))))
end
# TODO: @ref
macro default(thing)
  :(default_widget($(esc(thing))))
end

macro group(pr, thing)
  :(widget_group($(esc(pr)), $(esc(thing))))
end

macro splitv(things...)
  ex = :(default_layout(LayoutStyle(:split_vertical)))
  for thing in things
    push!(ex.args, esc(thing))
  end
  ex
end
macro splith(things...)
  ex = :(default_layout(LayoutStyle(:split_horizontal)))
  for thing in things
    push!(ex.args, esc(thing))
  end
  ex
end
# TODO: Support kwargs
macro plot(things...)
  ex = :(default_plot(PlotStyle(:plot)))
  for thing in things
    push!(ex.args, esc(thing))
  end
  ex
end
macro label(thing)
  :(default_label($(esc(thing))))
end

end
